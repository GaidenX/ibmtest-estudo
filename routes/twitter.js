const Twit = require('twit');
const ENV = require('dotenv');
ENV.config();

var nodeTwitter = new Twit({
    consumer_key: process.env.TWITTER_CONSUMER_KEY,
    consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
    access_token: process.env.TWITTER_ACCESS_TOKEN,
    access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
})

exports.nodeTwitterApi = function (request, response) {
    var paramsSearch = {
        q: '%23' + request.body.searchTerm,
        lang: 'en',
        count: 8,
        tweet_mode: 'extended'
    };

    nodeTwitter.get('search/tweets', paramsSearch, function (err, data) {
        for(var key in data.statuses){
            if(data.statuses[key].retweeted_status) {data.statuses[key].full_text = data.statuses[key].retweeted_status.full_text;}
            // console.log(data.statuses[key])
        }
        
        if(err) {
            console.log(err)
            response.status(400).send({
				error: err
			});
        }
        else{
            response.status(200).send({success : data});
        }  
    })
}