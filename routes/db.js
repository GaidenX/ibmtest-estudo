const mysql = require('mysql');
const ENV = require('dotenv');
ENV.config();

// Conect to SQL using Config
var conSql = mysql.createConnection({
    host: process.env.MYSQL_HOST,
    port: process.env.MYSQL_PORT,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_database
});

exports.sqlConfig = function () {
    conSql.connect(function (err) {
        if (err) {
            console.log(err);
            if (err.code === 'PROTOCOL_CONNECTION_LOST') { // See error protocol is connection lost
                sqlConfig();
            }
        } else {
            setInterval(function () {
                conSql.query("select * from ranking where feeling='angry'");
            }, 5000);
        }
    });
    return conSql;
}

exports.getRanking = function (request, response) {
    let sql = "select * from ranking";

    conSql.query(sql, function (err, result) {
        if (err || result.length == 0) {
            response.status(400).send({
                error: "Não foi possivel encontrar resultados no banco de dados"
            });
        } else {
            response.status(200).send(result);
        }
    });
}

exports.changeRanking = function (request, response) {
    // console.log(request.body);

    switch (request.body.action) {
        case "change":
            var sql = "update ranking set feeling=?,percentage=?,user=?,text=? where id=" + request.body.id;
            var values = [request.body.feeling, request.body.percentage, request.body.user, request.body.text];

            conSql.query(sql, values, function (err, result) {
                if (err) response.status(400).send({
                    error: err
                });
                else response.status(200).send({
                    status: 'Ranking alterado com sucesso, item modificado'
                });
            });
        break;
        case "new":
            var sql = "insert into ranking(feeling,percentage,user,text) values ?";
            var values = [[request.body.feeling, request.body.percentage, request.body.user, request.body.text]];

            conSql.query(sql, [values], function (err, result) {
                if (err) response.status(400).send({
                    error: err
                });
                else response.status(200).send({
                    status: 'Ranking alterado com sucesso, item adicionado'
                });
            });
        break;
    }
}