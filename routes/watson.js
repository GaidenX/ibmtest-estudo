const ENV = require('dotenv');
ENV.config();

const NaturalLanguageUnderstandingV1 = require('watson-developer-cloud/natural-language-understanding/v1.js');

var nlu = new NaturalLanguageUnderstandingV1({
    iam_apikey: process.env.WATSON_APIKEY,
    version: '2018-04-05',
    url: process.env.WATSON_URL
});

exports.nodeWatsonAnalyze = function (request, response) {
    let textToWatson = request.body.text;
    // console.log(textToWatson);

    return new Promise(function (resolve, reject) {
        nlu.analyze({
                text: textToWatson,
                features: {
                    sentiment: {},
                    emotion: {}
                }
            },
            function (err, response) {
                if (err) {
                    console.log('error:', err);
                } else {
                    // console.log(JSON.stringify(response, null, 2));
                    resolve(response);
                }
            }
        )
    })
}